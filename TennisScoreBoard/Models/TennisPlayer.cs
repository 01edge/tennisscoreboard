﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisScoreBoard.Models {
    internal class TennisPlayer {
        public bool HasAdvantage { get; set; }
        public bool HasGamePoint { get; set; }
        public bool HasMatchPoint { get; set; }
        public bool HasSetPoint { get; set; }
        public bool IsWinner { get; private set; }
        public string Name { get; set; }

        public int Points { get; set; }
        public int WonGames { get; set; }
        public int WonSets { get; set; }

        public TennisPlayer(string playerName) {
            this.Name = playerName;
        }

        public void GainsAdvantage() {
            HasAdvantage = true;
            HasGamePoint = false;
        }

        public void GainsGamePoint() {
            HasGamePoint = true;
            HasAdvantage = false;
        }

        public void GainsMatchPoint() {
            HasMatchPoint = true;
        }

        public void GainsSetPoint() {
            HasSetPoint = true;
        }

        public void LosesAdvantage() {
            HasAdvantage = false;
        }

        public void LosesGame() {
            HasAdvantage = false;
            HasGamePoint = false;
            Points = 0;
        }

        public void LosesGamePoint() {
            HasGamePoint = false;
        }

        public void LosesSet() {
            WonGames = 0;
            HasSetPoint = false;
            LosesGame();
        }

        public void LosesSetPoint() {
            HasSetPoint = false;
        }

        public void ScoresPoint() {
            switch (Points) {
                case 0:
                    Points = 15;
                    break;
                case 15:
                    Points = 30;
                    break;
                case 30:
                    Points = 40;
                    break;
            }
        }

        public void WinsGame() {
            WonGames++;
            LosesGame();
        }

        public void WinsSet() {
            WonSets++;
            LosesSet();
        }

        public void WonTotalGame() {
            IsWinner = true;
        }
    }
}

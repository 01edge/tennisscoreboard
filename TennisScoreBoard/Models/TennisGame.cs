﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisScoreBoard.Models {
    internal static class TennisGame {
        public static bool GameIsDeuce { get; set; }
    }

    internal enum ScoreType {
        Points, Games, Sets
    }
}

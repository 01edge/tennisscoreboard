﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisScoreBoard.Models;

namespace TennisScoreBoard.Services {
    internal static class FormattingService {
        private static StringBuilder builder = new StringBuilder();
        private static string CheckForAdvantage(bool playerHasAdvantage, int score) {
            return playerHasAdvantage == true ? "ADV" : score.ToString();
        }

        private static string DefaultScoreFormat(int player1Score, int player2Score, ScoreType scoreType) {
            return DefaultScoreFormat(player1Score, player2Score, false, false, scoreType);
        }

        private static string DefaultScoreFormat(int player1Score, int player2Score, bool player1Advantage, bool player2Advantage, ScoreType scoreType) {
            var player1ScoreNotation = player1Score.ToString();
            var player2ScoreNotation = player2Score.ToString();
            switch (scoreType) {
                case ScoreType.Points:
                    if(player1Advantage || player2Advantage) {
                        player1ScoreNotation = CheckForAdvantage(player1Advantage, player1Score);
                        player2ScoreNotation = CheckForAdvantage(player2Advantage, player2Score);
                    }else if (player1Score == 40 && player2Score == 40) {
                        TennisGame.GameIsDeuce = true;
                        return "DEUCE";
                    }
                    TennisGame.GameIsDeuce = false;
                    break;
                case ScoreType.Games:
                case ScoreType.Sets:
                    player1ScoreNotation = player1Score.ToString();
                    player2ScoreNotation = player2Score.ToString();
                    break;
            }
            return string.Format("{0}" + AppConstants.SCORESPACER + "{1}", player1ScoreNotation, player2ScoreNotation);
        }

        private static void GetGameScore(int player1WonGames, int player2WonGames) {
            builder.AppendLine(AppConstants.GAMESLABEL);
            builder.AppendLine(DefaultScoreFormat(player1WonGames, player2WonGames, ScoreType.Games));
        }

        private static void GetPointScore(int player1PointScore, int player2PointScore, bool player1HasAdvantage, bool player2HasAdvantage) {
            builder.AppendLine(AppConstants.POINTSLABEL);
            builder.AppendLine(DefaultScoreFormat(player1PointScore, player2PointScore, player1HasAdvantage, player2HasAdvantage, ScoreType.Points));
        }

        private static string GetScore(TennisPlayer player1, TennisPlayer player2) {
            GetPointScore(player1.Points, player2.Points, player1.HasAdvantage, player2.HasAdvantage);
            GetGameScore(player1.WonGames, player2.WonGames);
            GetSetScore(player1.WonSets, player2.WonSets);
            return builder.ToString();
        }

        private static void GetSetScore(int player1WonSets, int player2WonSets) {
            builder.AppendLine(AppConstants.SETSLABEL);
            builder.AppendLine(DefaultScoreFormat(player1WonSets, player2WonSets, ScoreType.Sets));
        }

        public static string TotalScoreFormatting(TennisPlayer player1, TennisPlayer player2) {
            builder.Clear();
            string playerName = null;
            if (player1.IsWinner) {
                playerName = player1.Name;
            } else if (player2.IsWinner) {
                playerName = player2.Name;
            } else {
                return GetScore(player1, player2);
            }
            return playerName + AppConstants.WEHAVEAWINNER;
        }
    }
}

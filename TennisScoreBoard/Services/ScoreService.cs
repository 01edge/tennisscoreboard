﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisScoreBoard.Models;
using Windows.UI.Xaml.Controls;

namespace TennisScoreBoard.Services {
    internal class ScoreService {
        private TennisPlayer _player1, _player2;
        private TextBlock CurrentScore;
        public ScoreService(TextBlock currentScoreTextBlockElement) {
            _player1 = new TennisPlayer("Player1");
            _player2 = new TennisPlayer("Player2");
            CurrentScore = currentScoreTextBlockElement;
        }

        public void AddPlayerPoint(string buttonName) {
            var player = GetPlayer(buttonName);
            player.ScoresPoint();
            CheckPointDifferenceRules(player, GetOpponent(buttonName));
            DisplayScore();
        }

        private void CheckGameDifferenceRules(TennisPlayer playerScoring, TennisPlayer playerNotScoring) {
            if (playerScoring.HasSetPoint) {
                playerScoring.WinsSet();
                playerNotScoring.LosesSet();
                CheckSetDifferenceRules(playerScoring, playerNotScoring);
            }else if(playerScoring.WonGames >= 6 && (playerScoring.WonGames - playerNotScoring.WonGames) >= 2) {
                playerScoring.GainsSetPoint();
            }
        }

        private void CheckPointDifferenceRules(TennisPlayer playerScoring, TennisPlayer playerNotScoring) {
            if (!TennisGame.GameIsDeuce && playerNotScoring.HasAdvantage) {
                playerNotScoring.LosesAdvantage();
            } else if (TennisGame.GameIsDeuce && !playerScoring.HasAdvantage) {
                playerScoring.GainsAdvantage();
            } else if (playerScoring.Points == 40 && !playerScoring.HasGamePoint 
                && playerNotScoring.Points <= 30 && !playerNotScoring.HasGamePoint) {
                playerScoring.GainsGamePoint();
            } else if (playerScoring.HasGamePoint || playerScoring.HasAdvantage) {
                playerScoring.WinsGame();
                playerNotScoring.LosesGame();
                CheckGameDifferenceRules(playerScoring, playerNotScoring);
            }
        }

        private void CheckSetDifferenceRules(TennisPlayer playerScoring, TennisPlayer playerNotScoring) {
            if (playerScoring.HasMatchPoint) {
                playerScoring.WonTotalGame();
            }else if(playerScoring.WonSets >= 3 && (playerScoring.WonSets - playerNotScoring.WonSets) >= 2) {
                playerScoring.GainsMatchPoint();
            }
        }

        public void DisplayScore() {
            CurrentScore.Text = FormattingService.TotalScoreFormatting(_player1, _player2);
        }

        private TennisPlayer GetOpponent(string buttonName) {
            return buttonName.Contains("Player1") ? _player2 : _player1;
        }

        private TennisPlayer GetPlayer(string buttonName) {
            return buttonName.Contains("Player1") ? _player1 : _player2;
        }
    }
}

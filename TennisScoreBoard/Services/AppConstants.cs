﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisScoreBoard.Services {
    internal static class AppConstants {
        public const string SCORESPACER = "          -          ";
        public const string SETSLABEL = "Sets: ";
        public const string POINTSLABEL = "Points: ";
        public static string GAMESLABEL = "Games: ";
        public static string WEHAVEAWINNER = " is the winner!!!";
    }
}
